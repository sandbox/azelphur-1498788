<?php

/**
 * @file
 * Sets up the base table for our entity and a table to store information about
 * the entity types.
 */

/**
 * Implements hook_install().
 */
function minecraft_account_install() {
  // Create Minecraft AvatarType
  if (!avatar_get_types('minecraft_account')) {
    $minecraft = new AvatarType();
    $minecraft->label = "Minecraft Account";
    $minecraft->type = "minecraft_account";
    $minecraft->module = "minecraft_account";
    avatar_type_save($minecraft);
  }
  // Create automatic fields
  $fields = minecraft_account_install_fields();
  foreach ($fields as $field) {
    if (!field_info_field($field['field_name'])) {
      field_create_field($field);
    }
  }
  
  // attach fields to minecraft_account type
  $instances = minecraft_account_install_instances();
  foreach ($instances as $instance) {
    if (!field_info_instance($instance['entity_type'], $instance['field_name'], $instance['bundle'])) {
      field_create_instance($instance);
    }
  }
}

/**
 * Implements hook_uninstall().
 * Delete all avatars created by this module as well a fields and instances if possible.
 */
function minecraft_account_uninstall() {
  
  $query = db_select('avatar', 'a');
  $query->fields('a', array('aid'));
  $query->condition('type', 'minecraft_account');
  $aids = $query->execute()->fetchCol();
  avatar_delete_multiple($aids);
  
  $instances = field_info_instances('avatar', 'minecraft_account');
  foreach ($instances as $instance) {
    field_delete_instance($instance, TRUE);
  }
  
  $fields = minecraft_account_install_fields();
  foreach ($fields as $key => $value) {
    field_delete_field($key);
  }
}

function minecraft_account_install_fields() {
	return array (
					'field_validated' => 
					array (
						'translatable' => '0',
						'entity_types' => 
						array (
						),
						'settings' => 
						array (
							'allowed_values' => 
							array (
								0 => '0',
								1 => '1',
							),
							'allowed_values_function' => '',
						),
						'storage' => 
						array (
							'type' => 'field_sql_storage',
							'settings' => 
							array (
							),
							'module' => 'field_sql_storage',
							'active' => '1',
							'details' => 
							array (
								'sql' => 
								array (
								  'FIELD_LOAD_CURRENT' => 
								  array (
								    'field_data_field_validated' => 
								    array (
								      'value' => 'field_validated_value',
								    ),
								  ),
								  'FIELD_LOAD_REVISION' => 
								  array (
								    'field_revision_field_validated' => 
								    array (
								      'value' => 'field_validated_value',
								    ),
								  ),
								),
							),
						),
						'foreign keys' => 
						array (
						),
						'indexes' => 
						array (
							'value' => 
							array (
								0 => 'value',
							),
						),
						'id' => '8',
						'field_name' => 'field_validated',
						'type' => 'list_boolean',
						'module' => 'list',
						'active' => '1',
						'locked' => '0',
						'cardinality' => '1',
						'deleted' => '0',
						'columns' => 
						array (
							'value' => 
							array (
								'type' => 'int',
								'not null' => false,
							),
						),
						'bundles' => 
						array (
							'avatar' => 
							array (
								0 => 'minecraft_account',
							),
						),
					),
					'field_portrait' => 
					array (
						'translatable' => '0',
						'entity_types' => 
						array (
						),
						'settings' => 
						array (
							'uri_scheme' => 'public',
							'default_image' => 0,
						),
						'storage' => 
						array (
							'type' => 'field_sql_storage',
							'settings' => 
							array (
							),
							'module' => 'field_sql_storage',
							'active' => '1',
							'details' => 
							array (
								'sql' => 
								array (
								  'FIELD_LOAD_CURRENT' => 
								  array (
								    'field_data_field_portrait' => 
								    array (
								      'fid' => 'field_portrait_fid',
								      'alt' => 'field_portrait_alt',
								      'title' => 'field_portrait_title',
								      'width' => 'field_portrait_width',
								      'height' => 'field_portrait_height',
								    ),
								  ),
								  'FIELD_LOAD_REVISION' => 
								  array (
								    'field_revision_field_portrait' => 
								    array (
								      'fid' => 'field_portrait_fid',
								      'alt' => 'field_portrait_alt',
								      'title' => 'field_portrait_title',
								      'width' => 'field_portrait_width',
								      'height' => 'field_portrait_height',
								    ),
								  ),
								),
							),
						),
						'foreign keys' => 
						array (
							'fid' => 
							array (
								'table' => 'file_managed',
								'columns' => 
								array (
								  'fid' => 'fid',
								),
							),
						),
						'indexes' => 
						array (
							'fid' => 
							array (
								0 => 'fid',
							),
						),
						'id' => '10',
						'field_name' => 'field_portrait',
						'type' => 'image',
						'module' => 'image',
						'active' => '1',
						'locked' => '0',
						'cardinality' => '1',
						'deleted' => '0',
						'columns' => 
						array (
							'fid' => 
							array (
								'description' => 'The {file_managed}.fid being referenced in this field.',
								'type' => 'int',
								'not null' => false,
								'unsigned' => true,
							),
							'alt' => 
							array (
								'description' => 'Alternative image text, for the image\'s \'alt\' attribute.',
								'type' => 'varchar',
								'length' => 512,
								'not null' => false,
							),
							'title' => 
							array (
								'description' => 'Image title text, for the image\'s \'title\' attribute.',
								'type' => 'varchar',
								'length' => 1024,
								'not null' => false,
							),
							'width' => 
							array (
								'description' => 'The width of the image in pixels.',
								'type' => 'int',
								'unsigned' => true,
							),
							'height' => 
							array (
								'description' => 'The height of the image in pixels.',
								'type' => 'int',
								'unsigned' => true,
							),
						),
						'bundles' => 
						array (
							'avatar' => 
							array (
								0 => 'minecraft_account',
							),
						),
					),
				);
}

function minecraft_account_install_instances() {
	return array (
					'field_validated' => 
					array (
						'label' => 'validated',
						'widget' => 
						array (
							'weight' => '1',
							'type' => 'options_buttons',
							'module' => 'options',
							'active' => 1,
							'settings' => 
							array (
							),
						),
						'settings' => 
						array (
							'user_register_form' => false,
						),
						'display' => 
						array (
							'default' => 
							array (
								'label' => 'above',
								'type' => 'list_default',
								'settings' => 
								array (
								),
								'module' => 'list',
								'weight' => 0,
							),
							'teaser' => 
							array (
								'type' => 'hidden',
								'label' => 'above',
								'settings' => 
								array (
								),
								'weight' => 0,
							),
							'popup' => 
							array (
								'type' => 'hidden',
								'label' => 'above',
								'settings' => 
								array (
								),
								'weight' => 0,
							),
							'select_block' => 
							array (
								'type' => 'hidden',
								'label' => 'above',
								'settings' => 
								array (
								),
								'weight' => 0,
							),
							'select_block_pulldown' => 
							array (
								'type' => 'hidden',
								'label' => 'above',
								'settings' => 
								array (
								),
								'weight' => 0,
							),
						),
						'required' => 1,
						'description' => '',
						'default_value' => 
						array (
							0 => 
							array (
								'value' => '0',
							),
						),
						'field_name' => 'field_validated',
						'entity_type' => 'avatar',
						'bundle' => 'minecraft_account',
						'deleted' => '0',
					),
					'field_portrait' => 
					array (
						'label' => 'portrait',
						'widget' => 
						array (
							'weight' => '3',
							'type' => 'image_image',
							'module' => 'image',
							'active' => 1,
							'settings' => 
							array (
								'progress_indicator' => 'throbber',
								'preview_image_style' => 'thumbnail',
							),
						),
						'settings' => 
						array (
							'file_directory' => '',
							'file_extensions' => 'png gif jpg jpeg',
							'max_filesize' => '',
							'max_resolution' => '',
							'min_resolution' => '',
							'alt_field' => 0,
							'title_field' => 0,
							'user_register_form' => false,
						),
						'display' => 
						array (
							'default' => 
							array (
								'label' => 'above',
								'type' => 'image',
								'settings' => 
								array (
								  'image_style' => '',
								  'image_link' => '',
								),
								'module' => 'image',
								'weight' => 2,
							),
							'teaser' => 
							array (
								'type' => 'hidden',
								'label' => 'above',
								'settings' => 
								array (
								),
								'weight' => 0,
							),
							'popup' => 
							array (
								'type' => 'hidden',
								'label' => 'above',
								'settings' => 
								array (
								),
								'weight' => 0,
							),
							'select_block' => 
							array (
								'type' => 'hidden',
								'label' => 'above',
								'settings' => 
								array (
								),
								'weight' => 0,
							),
							'select_block_pulldown' => 
							array (
								'type' => 'hidden',
								'label' => 'above',
								'settings' => 
								array (
								),
								'weight' => 0,
							),
						),
						'required' => 0,
						'description' => '',
						'field_name' => 'field_portrait',
						'entity_type' => 'avatar',
						'bundle' => 'minecraft_account',
						'deleted' => '0',
					),
				);
}

